/**
 * Plexi - Living Backlit Plexi Prints
 *  
 */

#include "SCoopME.h"
#include <Streaming.h>

// INPUTS
const int inLightSensorPin = A0;
const int inPirSensorPin = 7;

// OUTPUT
const int plexiLedPin = 9;
const int yellowLedPin = 10;
const int redLedPin = 11;

// SENSOR VALUES
boolean motion = false;
unsigned long lastMotionTime = 0;
int light = 0;

// STATE MACHINE VALUES
boolean adjusting = false;
int plexiCurrent = 0;
int plexiTarget = 0;
// linear trajectory
const int linearIncrements = 1;
const int triggerThreshold = 1;
const int linearPeriod = 100;
// Fade off
const long fadeOffTimeout = 5000; // 30 seconds

/** 
 * Adjusts a value base on a linear trajectory
 * Returns adjusted value
 */
int adjustLinear(int current, int target, int inc, int threshold) {
  int diff = 0;
  int totalDiff = target - current;
  // check if adjustment is needed
  if (abs(totalDiff) >= threshold) {
    // calculate adjustment (negative or positive)
    if (totalDiff > 0) {
      diff = min(inc, totalDiff);
    }
    else {
      diff = -max(inc, totalDiff); 
    }
  }
  int newValue = current + diff;
  return newValue;
}

// First Task
struct adjustPlexiTask : 
SCoopTask< adjustPlexiTask > {	// default stack (150bytes) and quantum time (400us)
  static void setup() { 
  }
  static void loop() {
    // Dont forget: call yield() or sleep(xx) from time to time
    int newPlexiCurrent = adjustLinear(plexiCurrent, plexiTarget, linearIncrements, triggerThreshold);
    // yellow adjusting Led
    adjusting = newPlexiCurrent != plexiCurrent;
    // write the Plexi Brightness value
    analogWrite(plexiLedPin, newPlexiCurrent);
    plexiCurrent = newPlexiCurrent;
    sleep(linearPeriod);
  } 
} 
adjustPlexiTask;

/**
 * Setup
 */
void setup()
{
  // user setup here below. tasks will be automatically setup() by next call to yield.
  Serial.begin(9600);  //Begin serial communcation
  // PIR will be a digital input
  pinMode(inPirSensorPin, INPUT);
  // Light sensor (Photo resistor)
  pinMode(inLightSensorPin, INPUT);
  // 2 output leds
  pinMode(plexiLedPin, OUTPUT);
  pinMode(redLedPin, OUTPUT);
  pinMode(yellowLedPin, OUTPUT);
}

/**
 * Main Arduino loop
 */
void loop() {
  //Serial.println("loop");
  yield();  // orchestrate everything.
  // update sensor values from environment
  readSensors();
  // update the state machine leds
  updateLeds();
  // debug output
  serialOutput();
}

/**
 * Read the motion and light sensors 
 */
void readSensors() {
  // read motion detector (PIR)
  boolean currentMotion = digitalRead(inPirSensorPin) == HIGH;
  // What the sensor triggered just now
  boolean motionJustDetected = !motion && currentMotion;
  motion = currentMotion;

  // read from the light sensor (photo resistor)
  light = analogRead(inLightSensorPin);
  // if motion was just detected, set the new target from the light sensor
  if (motionJustDetected) {
    Serial << "MOTION - Adjusting to ambient light" << endl;
    plexiTarget = constrain( map(light, 0, 1024, 0, 350) , 0, 255);;
  }
  // update motion time or fade off if no motion for too long
  if (motion) {
    lastMotionTime = millis();
  }
  else {
    // TODO check for overflow (every 50 days)
    long timeSinceLastMotion = millis() - lastMotionTime;
    if (timeSinceLastMotion > fadeOffTimeout) {
      if (!adjusting && (plexiCurrent !=0)) {
        Serial << "FADE OUT - time since last motion > " << fadeOffTimeout / 1000 << " seconds" << endl;
        plexiTarget = 0;
      }
    }
  }
  
  // Add Random Trajectories
  // - Sine for a while
  // - Random Bursts of light
  // - Burst to fade
  // - Large change in lighting will trigger adjustment
  // DONE - No motion for long time, fade off
  
}

/**
 * Update the LEDs
 */
void updateLeds() {
  // Red is motion
  digitalWrite(redLedPin, motion);
  // yellow is "Adjusting"
  digitalWrite(yellowLedPin, adjusting);
}


/**
 * Print out to Serial
 */
void serialOutput() {
  // update serial
  Serial << "motion:" << motion << " light:" << light << " plexi:" << plexiCurrent;
  if (adjusting) {
    Serial << " [adjusting to target:" << plexiTarget << "]";
  }
  else {
     Serial << " [not adjusting]";
  }
  Serial << endl; 
}




