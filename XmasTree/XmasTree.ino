#include "Adafruit_WS2801.h"
#include "SPI.h" // Comment out this line if using Trinket or Gemma
#ifdef __AVR_ATtiny85__
 #include <avr/power.h>
#endif

#include <QueueArray.h>

uint8_t dataPin  = 2;    // Yellow wire on Adafruit Pixels
uint8_t clockPin = 3;    // Green wire on Adafruit Pixels

const int rainbowButtonPin = 5;

// Don't forget to connect the ground wire to Arduino ground,
// and the +5V wire to a +5V supply

uint8_t PIXEL_COUNT = 50;

// Set the first variable to the NUMBER of pixels. 25 = 25 pixels in a row
Adafruit_WS2801 strip = Adafruit_WS2801(PIXEL_COUNT, dataPin, clockPin);

const int RANDOM_COUNT = 7;
QueueArray <int> queue;

void setup() {
  #if defined(__AVR_ATtiny85__) && (F_CPU == 16000000L)
    clock_prescale_set(clock_div_1); // Enable 16 MHz on Trinket
  #endif

  strip.begin();
  // Update LED contents, to start they are all 'off'
  strip.show();
  // init random
  randomSeed(analogRead(0));
  // input buttons
  pinMode(rainbowButtonPin, INPUT_PULLUP);
}



void loop() {
  // read the state of the pushbutton value:
  int rainbowButtonState = digitalRead(rainbowButtonPin);
  if (rainbowButtonState == LOW) {
    rainbowCycle(0);
    colorFill(Color(0, 0, 0), true);
  }
  else {
    randomPixels2014();
    delay(200);
  }
  
  // WALKING  
  /* uint32_t base = Color(10, 0, 0);
  uint32_t walkColor = Color(0, 10, 0);
  int d = 100;
  walkingPixel(base, walkColor, d);
  */
  
  // FILL
  //colorFill(Color(10, 0, 0), true);
  //delay(1000);
  //colorFill(Color(0, 10, 0));
  //delay(100);
  
  //colorWipe(Color(87, 10, 203), 20);
  //colorWipe(Color(255, 0, 0), 30);
  //colorWipe(Color(255, 255, 0), 40);
  //colorWipe(Color(255, 255, 0), 40);
  //setPixel(49, 25, 0, 0);
  
  //strip.setBrightness(10);

  //rainbow(20);
  //rainbowCycle(0);
  
}


uint32_t walkingPixelIndex = 0;

void walkingPixel(uint32_t baseColor, uint32_t walkerColor, int delayTime) {
  colorFill(baseColor, false);
  int beforeIndex = max(0, walkingPixelIndex-1);
  int afterIndex = min(PIXEL_COUNT, walkingPixelIndex+1);
  setPixel(walkingPixelIndex, walkerColor);
  //setPixel(beforeIndex, Color(5, 5, 0));
  //setPixel(afterIndex, Color(5, 5, 0));
  walkingPixelIndex++;
  if (walkingPixelIndex >= PIXEL_COUNT) {
      walkingPixelIndex = 0;
  }
  delay(delayTime);
}


/**
 * Iterate once in the Random Pixel cycle
 */
void randomPixels() {
  int pixelIndex = randomPixel();
  queue.push(pixelIndex);
  if (queue.count() > RANDOM_COUNT) {
     int whitePixel = queue.pop();
     setPixel(whitePixel, 0, 0, 0);
  }
}

/**
 * Iterate once in the Random Pixel cycle
 */
void randomPixels2014() {
  int pixelIndex = randomPixel2014();
  queue.push(pixelIndex);
  if (queue.count() > RANDOM_COUNT) {
     int whitePixel = queue.pop();
     setPixel(whitePixel, 0, 0, 0);
  }
}

int randomPixel() {
 int pixelIndex = random(PIXEL_COUNT);
 int randomRed = random(256); 
 int randomGreen = random(256);
 int randomBlue = random(256);
 setPixel(pixelIndex, randomRed, randomGreen, randomBlue);
 return pixelIndex;
}

/**
 * Random Pixel with Flashier colors
 */
int randomPixel2014() {
 int pixelIndex = random(PIXEL_COUNT);
 int randomRed = random(256); 
 int randomGreen = random(256-randomRed);
 int randomBlue = random(256-randomRed-randomGreen);
 setPixel(pixelIndex, randomRed, randomGreen, randomBlue);
 return pixelIndex;
}

void setPixel(int index, uint32_t color) {
  strip.setPixelColor(index, color);
  strip.show(); 
}

void setPixel(int index, byte r, byte g, byte b) {
  uint32_t c = Color(r, g, b);
  strip.setPixelColor(index, c);
  strip.show(); 
}

void rainbow(uint8_t wait) {
  int i, j;
   
  for (j=0; j < 256; j++) {     // 3 cycles of all 256 colors in the wheel
    for (i=0; i < strip.numPixels(); i++) {
      strip.setPixelColor(i, Wheel( (i + j) % 255));
    }  
    strip.show();   // write all the pixels out
    delay(wait);
  }
}

// Slightly different, this one makes the rainbow wheel equally distributed 
// along the chain
void rainbowCycle(uint8_t wait) {
  int i, j;
  
  for (j=0; j < 256 * 2; j++) {     // 5 cycles of all 25 colors in the wheel
    for (i=0; i < strip.numPixels(); i++) {
      // tricky math! we use each pixel as a fraction of the full 96-color wheel
      // (thats the i / strip.numPixels() part)
      // Then add in j which makes the colors go around per pixel
      // the % 96 is to make the wheel cycle around
      strip.setPixelColor(i, Wheel( ((i * 256 / strip.numPixels()) + j) % 256) );
    }  
    strip.show();   // write all the pixels out
    delay(wait);
  }
}

// fill the dots one after the other with said color
// good for testing purposes
void colorWipe(uint32_t c, uint8_t wait) {
  int i;
  
  for (i=0; i < strip.numPixels(); i++) {
      strip.setPixelColor(i, c);
      strip.show();
      delay(wait);
  }
}

/**
 * All LEDs one color
 */
void colorFill(uint32_t c, boolean callShow) {
  int i;  
  for (i=0; i < strip.numPixels(); i++) {
      strip.setPixelColor(i, c);
  }
  if (callShow) {
    strip.show();
  }
}

/* Helper functions */

// Create a 24 bit color value from R,G,B
uint32_t Color(byte r, byte g, byte b)
{
  uint32_t c;
  c = r;
  c <<= 8;
  c |= g;
  c <<= 8;
  c |= b;
  return c;
}

//Input a value 0 to 255 to get a color value.
//The colours are a transition r - g -b - back to r
uint32_t Wheel(byte WheelPos)
{
  if (WheelPos < 85) {
   return Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  } else if (WheelPos < 170) {
   WheelPos -= 85;
   return Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else {
   WheelPos -= 170; 
   return Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
}
