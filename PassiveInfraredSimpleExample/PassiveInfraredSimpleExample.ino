
/**
 * Simple example of reading from a Passive IR Sensor
 */

// Connect PIR miidle pin to pin 7
const int inPirSensorPin = 7;
const int redLedPin = 11;

void setup() {
  // initialize serial com
  Serial.begin(9600);
  // PIR will be a digital input
  pinMode(inPirSensorPin, INPUT);
  // Red LED is an output
  pinMode(redLedPin, OUTPUT);
}

void loop() {
  // read the sensor:
  //int pirSensorValue = analogRead(pirSensorPin);
  int pirSensorValue = digitalRead(inPirSensorPin);
  if (pirSensorValue == HIGH) {
    analogWrite(redLedPin, 255);
  }
  else {
    analogWrite(redLedPin, 0);
  }
  // print out to serial
  //Serial.print("PIR value:"); Serial.println(pirSensorValue);
  // wait 100 ms
  delay(100);
}
