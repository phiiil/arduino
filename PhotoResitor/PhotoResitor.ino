#include <Streaming.h>

int lightPin = A0;  //define a pin for Photo resistor
int ledPin = 11;     //define a pin for LED

void setup()
{
    Serial.begin(9600);  //Begin serial communcation
    pinMode( ledPin, OUTPUT );
    pinMode( lightPin, INPUT );
}

#define rate     1
int ledCurrent = 0;
int ledTarget = 0;

/** Main Loop */
void loop() {
  ambiantLightDimmer();
}

void ambiantLightDimmer() 
{
    // read from the light sensor (photo resistor)
    int light = analogRead(lightPin);
    // map the light sensor to the led brightness 
    int ledTarget = constrain( map(light, 0, 1024, 0, 350) , 0, 255);
    
    int ledDiff = ledTarget - ledCurrent;
    // smoothing
    int ledDiffNow = 0;
    if (abs(ledDiff) >= (rate*5)) {
      if (ledDiff > 0) {
        ledDiffNow = rate;
      }
      else {
         ledDiffNow = -rate; 
      }
    }
    else {
      //Serial << "skipped" << endl;
    }
    ledCurrent = ledCurrent + ledDiffNow;
    
    // write the led value
    analogWrite(ledPin, ledCurrent);
    // log
    Serial << "light:" << light << "  led:" << ledCurrent << "  ledDiff:" << ledDiff << "  ledDiffNow:" << ledDiffNow << "  ledTarget:" << ledTarget << endl;
    
    delay(20); //short delay for faster response to light.
}
